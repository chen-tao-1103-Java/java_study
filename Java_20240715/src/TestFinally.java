import java.util.InputMismatchException;
import java.util.Scanner;

public class TestFinally {
    public static int getData1() {
        Scanner sc = null;
        try {
            sc = new Scanner(System.in);
            int data = sc.nextInt();
            return data;
        } catch (InputMismatchException e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally中代码");
            //一定会被执行
        }

        System.out.println("try-catch-finally之后代码");
        if (null != sc) {
            sc.close();
        }
        return 0;
    }

    public static int getData2() {
        Scanner sc = null;
        try {
            sc = new Scanner(System.in);
            int data = sc.nextInt();
            return data;
        } catch (InputMismatchException e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally中代码");
            //一定会被执行
            sc.close();
        }
        System.out.println("try-catch-finally之后代码");
        return 0;
    }


    public static int getData() {
        Scanner sc = null;
        try {
            sc = new Scanner(System.in);
            int data = sc.nextInt();//10
            return data;
        } catch (InputMismatchException e) {
            e.printStackTrace();
        } finally {
            return 100;//建议 不要再finally中 return数据
        }
    }


    public static void main(String[] args) {
        try {
            func();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        System.out.println("after try catch");
    }

    public static void func() {
        int[] arr = {1, 2, 3};
        System.out.println(arr[100]);
    }


    public static void main1(String[] args) {
        int data = getData();
        System.out.println(data);
    }
}
