package inherit1;

public class CreditAccount extends Account {
    //信誉度
    private double credit;

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
}
