package inherit1;

public class AccountTest {
    public static void main(String[] args) {
        CreditAccount creditAccount = new CreditAccount();
        creditAccount.setAccountNo("123456789");
        creditAccount.setBalance(9000.00);
        System.out.println(creditAccount.getAccountNo() +
                " 信用账户，余额" + creditAccount.getBalance() + "元");
    }
}
