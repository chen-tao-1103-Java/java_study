package inherit1;

public class Account {
    //账号
    private String accountNo;

    //余额
    private double balance;


    //账号和余额的set和get方法
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
