package test;

public class Student {
    public String name;
    public int age;

    public static String classes = "计算机2班";//班级  静态成员变量 访问：类名.去访问

    {
        name = "李四";

        System.out.println("实例代码块！");
    }


    static {
        classes = "软件工程1班";
        //类加载的时候 就被执行
        System.out.println("静态代码块！");
    }


    public Student() {
        //调用本类当中 其他的构造方法
        //this("张三",99);//必须放在构造方法里面，并且必须是第一行
        System.out.println("不带参数的构造方法");
    }

    public Student(String name, int age) {
        //this(); 不能循环调用
        System.out.println("带2个参数的构造方法");
        this.name = name;
        this.age = age;
    }

    private void setStudent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void print() {
        System.out.println(this.name + " => " + this.age + "->" + Student.classes);
    }

    public static void func2() {
        //this.print();
    }
}
