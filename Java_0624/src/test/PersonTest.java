package test;

public class PersonTest {
    public static void main(String[] args) {
        //实例化对象
        Person person = new Person();
        //设置姓名
        person.setName("张三");
        //设置年龄
        person.setAge(18);
        //获取到名字
        System.out.println(person.getName());
        //获取到年龄
        System.out.println(person.getAge());
        person.show();
    }
}
