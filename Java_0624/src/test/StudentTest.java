package test;

public class StudentTest {
    public static void main(String[] args) {
        Student student1 = new Student("张三", 12);

        Student student2 = new Student("李四", 22);

        Student student3 = new Student("王五", 82);

        Student.classes = "计科1班";

        student1.print();
        student2.print();
        student3.print();

//        Student.classes = "软件工程2班";
//        Student.func2();
    }
}
