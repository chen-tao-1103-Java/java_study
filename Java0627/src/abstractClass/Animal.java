package abstractClass;

abstract public class Animal {
    //属性
    private String name;
    private int age;

    //构造方法
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //抽象方法
    abstract public void eat();

    abstract public void sleep();

    abstract public void walk();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
