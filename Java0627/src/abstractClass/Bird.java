package abstractClass;

public class Bird extends Animal {

    public Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + " is eating");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + " is sleeping");
    }

    @Override
    public void walk() {
        System.out.println(this.getName() + " is walking");
    }

    public void sing() {
        System.out.println(this.getName() + " is singing");
    }
}
