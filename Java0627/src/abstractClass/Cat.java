package abstractClass;

public class Cat extends Animal {

    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + " is eating");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + " is sleeping");
    }

    @Override
    public void walk() {
        System.out.println(this.getName() + " is walking");
    }


    public void catchMouse() {
        System.out.println(this.getName() + " is catching mouse");
    }
}
