package abstractClass;

public class Test {
    public static void eat(Animal animal) {
        animal.eat();
    }

    public static void main(String[] args) {
        eat(new Cat("猫咪", 5));
        eat(new Bird("自由哥", 3));
        eat(new Cat("猫咪", 5));
        System.out.println("===============");

        Animal[] animal = {
                new Dog("小七", 2),
                new Bird("自由哥", 3),
                new Cat("猫咪", 5)
        };

        for (Animal a : animal) {
            a.eat();
            a.sleep();
            a.walk();
        }
    }
}
