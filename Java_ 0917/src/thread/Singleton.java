package thread;

public class Singleton {
    private static Singleton instance = new Singleton();

    public static Singleton getInstance() {
        return instance;
    }


    private Singleton() {
    }


    public static void main(String[] args) {
        Singleton s = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        // Singleton s3 = new Singleton();
        System.out.println(s == s2);
    }
}

