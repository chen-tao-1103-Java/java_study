package sort;

public class MySort {
    public void quickSortHoare(int[] array) {
        quick(array, 0, array.length - 1);
    }

    private void quick(int[] array, int start, int end) {
        //递归结束条件
        if (start >= end) {
            return;
        }
        //递归到小的区间时,可以考虑使用插入排序
        if (end - start + 1 < 10) {
            //对start到end区间进行插入排序
            insertionSort(array, start, end);
            return;
        }
        //三数取中法
        //得到三个数中,中间大的值的索引
        int index = findIndexOfMidVal(array, start, end);

        //拿这个中间大的数和start下标的值交换,先让他做基准
        swap(array, index, start);

        //找新的基准(调整基准)
        int pivot = partitionHoare(array, start, end);
        quick(array, start, pivot);
        quick(array, pivot + 1, end);
    }

    //找三数当中,中间大的值
    private int findIndexOfMidVal(int[] array, int start, int end) {
        int mid = (start + end) / 2;
        if (array[start] < array[end]) {
            if (array[mid] < array[start]) {
                return start;
            } else if (array[mid] > array[end]) {
                return end;
            } else {
                return mid;
            }

        } else {
            if (array[mid] < array[end]) {
                return end;
            } else if (array[mid] > array[start]) {
                return start;
            } else {
                return mid;
            }
        }
    }

    private int partitionHoare(int[] array, int left, int right) {
        int i = left;
        while (left < right) {
            //假设基准就是第一个元素
            int pivot = array[left];
            while (left < right && array[right] > pivot) {
                right--;
            }
            while (left < right && array[left] < pivot) {
                left++;
            }
            swap(array, left, right);
        }
        swap(array, left, i);
        return left;
    }

    private void swap(int[] array, int left, int i) {
        int tmp = array[left];
        array[left] = array[i];
        array[i] = tmp;
    }

    private void insertionSort(int[] arr, int left, int right) {
        for (int i = left + 1; i <= right; i++) {
            int tmp = arr[i];
            int j = i - 1;
            for (; j >= left; j--) {
                if (arr[j] > tmp) {
                    arr[j + 1] = arr[j];
                } else {
                    break;
                }
            }
            arr[j + 1] = tmp;
        }
    }
}
