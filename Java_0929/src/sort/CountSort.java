package sort;

import java.util.Arrays;

public class CountSort {
    public static void countSort(int[] array) {
        //1、遍历数组 找到 最小值 和最大值   才能确定 计数数组的大小
        int maxVal = array[0];
        int minVal = array[0];
        //O(n)
        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxVal) {
                maxVal = array[i];
            }
            if (array[i] < minVal) {
                minVal = array[i];
            }
        }
        //2、确定计数数组的长度
        int len = maxVal - minVal + 1;
        int[] countArr = new int[len];
        //3. 开始遍历 当前数组 统计每个数字出现的次数  O(n)
        for (int i = 0; i < array.length; i++) {
            int val = array[i];
            countArr[val - minVal]++;//??????????????
        }

        int index = 0;
        //4. 遍历计数数组，看每个下标的值是几，就打印几个下标的数据就好了 O(范围 + n)
        //范围遍历一次，位置上所有的数的个数加起来等于n
        for (int i = 0; i < countArr.length; i++) {
            while (countArr[i] > 0) {
                //不敢打印
                array[index] = i + minVal;//??????????????
                index++;
                countArr[i]--;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {188, 2, 19, 134, 29, 15};
        countSort(array);
        System.out.println("排序后：" + Arrays.toString(array));
    }
}
