package sort;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[] array = {188, 2, 19, 134, 29, 15};
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.bubbleSort(array);
        System.out.println(Arrays.toString(array));

        HeapSort heapSort = new HeapSort();
        heapSort.heapSort(array);
        System.out.println(Arrays.toString(array));

        MergeSort sort = new MergeSort();
        sort.mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
}
