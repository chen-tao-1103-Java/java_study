public class Test {
    public static void main(String[] args) {
        MySingleList mySingleList = new MySingleList();
        mySingleList.createList();

        mySingleList.display();
        System.out.println(mySingleList.size());

        mySingleList.addFirst(100);
        mySingleList.addLast(200);
        mySingleList.addIndex(0, 1000);
    }
}
