import java.util.Arrays;

public class StrTest {
    public static void main(String[] args) {
        String str = "abcabcdabcdef";
        System.out.println(str.endsWith("cdef9"));//是否以这个字符串结尾的
        System.out.println(str.startsWith("abc"));
    }

    public static void main14(String[] args) {
        String str = " hello hello      ";
        boolean flg = str.contains("hello");
        System.out.println(flg);

        /*String ret = str.trim();//去除掉左右两边的空格
        System.out.print(ret);
        System.out.println("pppppp");*/
    }

    public static void main13(String[] args) {
        String str = "helloworld";
        //String ret1 = str.substring(1);
        String ret1 = str.substring(1, 4);//[1,4)
        // 字符串当中的库函数 基本上只要改变 返回的都是一个全新的对象
        System.out.println(ret1);
    }

    public static void main12(String[] args) {
        String str4 = "name=zhangsan&age=18";
        String[] ret5 = str4.split("&");
        for (String x : ret5) {
            //System.out.println(x);//name=zhangsan
            String[] ss = x.split("=");
            for (String s : ss) {
                System.out.println(s);
            }
        }

        String str3 = "127\\0\\0\\1";
        String[] ret4 = str3.split("\\\\");

        for (String x : ret4) {
            System.out.println(x);
        }
    }

    public static void main10(String[] args) {
        String str = "zhangsan&wangwu&zhaoliu&lisi";
        String[] ret = str.split("&", 2);
        System.out.println(Arrays.toString(ret));

        System.out.println("====================");
        String str2 = "127.0.0.1";
        String[] ret2 = str2.split("\\.");
        for (String x : ret2) {
            System.out.println(x);
        }

        System.out.println("====================");
        String str3 = "127\\0\\0\\1";
        // String[] ret3 = str3.split("\\\\");
        String[] ret4 = str3.split("/");//    \\
        for (String x : ret4) {
            System.out.println(x);
        }

        System.out.println("====================");

        String str4 = "name=zhangsan&age=18";
        String[] ret5 = str4.split("=|&");
        for (String x : ret5) {
            System.out.println(x);
        }
    }

    public static void main9(String[] args) {
        String str1 = "ababcabcdabcdef";
        /*String ret = str1.replace('a','p');
        System.out.println(ret);

        String ret2 = str1.replace("ab","pk");
        System.out.println(ret2);

        String ret = str1.replaceAll("abc","pkrg");
        System.out.println(ret);
*/
        String ret = str1.replaceFirst("abc", "pkrg");
        System.out.println(ret);
    }

    public static void main8(String[] args) {
        String str = "helloGT高";
        String ret = str.toUpperCase();
        System.out.println(ret);

        String str2 = "ABCDEFG";
        String ret2 = str2.toLowerCase();
        System.out.println(ret2);

        System.out.println("==============");
        char[] chars = str2.toCharArray();
        System.out.println(Arrays.toString(chars));

        String ret3 = String.format("%d,%d,%d", 2012, 12, 12);
        System.out.println(ret3);
    }

    public static void main7(String[] args) {
        String str = String.valueOf(123);
        System.out.println(str);
        String str2 = String.valueOf(true);
        System.out.println(str2);

        System.out.println("=============");
        //int val1 = Integer.parseInt("123");
        int val1 = Integer.valueOf("123");
        System.out.println(val1 + 1);

        double val2 = Double.parseDouble("12.25");
        System.out.println(val2);
    }


    public static void main6(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }
    }

    public static void main5(String[] args) {
        String str = "ababcabcd";
        /*for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            System.out.println(ch);
        }*/
        System.out.println(str.indexOf('c')); //从头开始找，遇到第一个就结束
        System.out.println(str.indexOf('c', 3)); //从头开始找，遇到第一个就结束
        System.out.println(str.indexOf("abc"));
        System.out.println(str.indexOf("abc", 4));

        System.out.println("========================");
        System.out.println(str.lastIndexOf('c'));//从后往前找
        System.out.println(str.lastIndexOf('c', 3));//从后往前找
        System.out.println(str.lastIndexOf("abc"));//从后往前找   5
        System.out.println(str.lastIndexOf("abc", 3));//从后往前找 2
    }

    public static void main4(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("Hello");

        //int ret = s1.compareTo(s2);//s1和s2比较
        int ret = s1.compareToIgnoreCase(s2);//s1和s2 忽略大小写进行 比较

        if (ret > 0) {
            System.out.println("s1>s2");
        } else if (ret == 0) {
            System.out.println("s1==s2");
        } else {
            System.out.println("s1<s2");
        }
    }

    public static void main3(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("hello");

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1.equalsIgnoreCase(s2));//忽略大小写 是否相同


    }

    public static void main2(String[] args) {
        String s1 = new String("hello");
        String s2 = new String("world");

        String s3 = s1;
        System.out.println(s3);
        System.out.println(s1.length());
    }

    public static void main1(String[] args) {
        String str1 = "hello";
        System.out.println(str1);

        String str2 = new String("abc");
        System.out.println(str2);

        char[] chars = {'g', 'a', 'o'};//字符数组
        String str3 = new String(chars);
        System.out.println(str3);
    }
}
