public class Test {
    public static void main(String[] args) {
        BinaryTree testBinaryTree = new BinaryTree();
        BinaryTree.TreeNode root = testBinaryTree.createTree();

        testBinaryTree.preOrder(root);
        System.out.println();
        testBinaryTree.inOrder(root);
        System.out.println();
        testBinaryTree.postOrder(root);
        System.out.println();
    }
}
