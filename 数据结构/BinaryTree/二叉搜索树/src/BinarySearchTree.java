/**
 * 模拟实现一颗简单的二叉搜索树
 */
public class BinarySearchTree {

    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root = null;

    /**
     * @param val:在二叉搜索树种搜索val
     * @return 找到了就返回节点, 没有找到就返回null
     */

    public TreeNode search(int val) {
        TreeNode cur = root;
        while (cur != null) {
            if (cur.val < val) {
                cur = cur.right;
            } else if (cur.val > val) {
                cur = cur.left;
            } else {
                return cur;
            }
        }
        return null;
    }

    /**
     * @param val:插入val
     * @return: 插入成功返回true, 否则返回false
     */
    public boolean insert(int val) {
        if (root == null) {
            root = new TreeNode(val);
            return true;
        }
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null) {
            if (cur.val < val) {
                parent = cur;
                cur = cur.right;
            } else if (cur.val > val) {
                parent = cur;
                cur = cur.left;
            } else {
                return false;
            }
        }
        //走到这里cur为空,这里就是要插入的位置
        TreeNode node = new TreeNode(val);
        if (val < parent.val) {
            parent.left = node;
        } else {
            parent.right = node;
        }
        return true;
    }

    public void inorder(TreeNode root) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }


    public void remove(int key) {
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null) {
            if (cur.val == key) {
                removeNode(parent, cur);
                return;
            } else if (cur.val < key) {
                parent = cur;
                cur = cur.right;
            } else {
                parent = cur;
                cur = cur.left;
            }
        }
    }

    private void removeNode(TreeNode parent, TreeNode cur) {
        if (cur.left == null) {
            //分三种情况
            if (cur == root) {
                root = cur.right;
            } else if (cur == parent.left) {
                parent.left = cur.right;
            } else {
                parent.right = cur.right;
            }

        } else if (cur.right == null) {
            //分三种情况
            if (cur == root) {
                root = cur.left;
            } else if (cur == parent.left) {
                parent.left = cur.left;
            } else {
                parent.right = cur.left;
            }

        } else {
            //左右都不为空
            TreeNode target = cur.right;//目标值是cur的右树
            TreeNode targetParent = cur;
            while (target.left != null) {
                targetParent = target;
                target = target.left;
            }
            //找到了目标值
            cur.val = target.val;
            if (target == targetParent.left) {
                targetParent.left = target.right;
            } else {
                //这种情况是为了去cur的右树中找最小值,发现最小值为null,即最小值不存在的情况
                targetParent.right = target.right;
            }
        }
    }

    public static void main(String[] args) {
        BinarySearchTree searchTree = new BinarySearchTree();
        int[] arr = {5, 3, 4, 1, 7, 8, 2, 6, 0, 9};
        for (int i = 0; i < arr.length; i++) {
            searchTree.insert(arr[i]);
        }
        searchTree.inorder(searchTree.root);
    }
}
