
import java.util.*;

public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        LinkedList<Integer> list = new LinkedList<>(arrayList);

        System.out.println(list);


        LinkedList<Integer> list2 = new LinkedList<>();
        list2.add(10);//默认他是尾插法
        list2.add(20);
        list2.add(30);
        list2.add(2, 19);

        System.out.println(list2);

        System.out.println("============");

        for (int x : list2) {
            System.out.print(x + " ");
        }
        System.out.println();
        System.out.println("======ListIterator======");
        ListIterator<Integer> listIterator = list2.listIterator();
        while (listIterator.hasNext()) {
            System.out.print(listIterator.next() + " ");
        }
        System.out.println();
        System.out.println("======ListIterator2======");
        ListIterator<Integer> listIterator2 = list2.listIterator(list2.size());
        while (listIterator2.hasPrevious()) {
            System.out.print(listIterator2.previous() + " ");
        }
        System.out.println();
    }
}
