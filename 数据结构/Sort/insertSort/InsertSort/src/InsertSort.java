public class InsertSort {
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int k = i - 1;
            for (; k >= 0; k--) {
                if (array[k] > tmp) {
                    array[k + 1] = array[k];
                } else {
                    break;
                }
            }
            array[k + 1] = tmp;
        }
    }
}
