import java.util.Arrays;

public class InsertSortTest {
    public static void main(String[] args) {
        int[] array = {5, 1, 3, 28, 9, 2};
        InsertSort.insertSort(array);
        System.out.println(Arrays.toString(array));
    }
}
