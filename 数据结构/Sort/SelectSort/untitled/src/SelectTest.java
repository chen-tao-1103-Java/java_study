import java.util.Arrays;

public class SelectTest {
    public static void main(String[] args) {
        SelectSort sort = new SelectSort();
        int[] array = {5, 1, 3, 28, 9, 2};

        SelectSort.selectSort(array);
        System.out.println(Arrays.toString(array));

        SelectSort.selectSort2(array);
        System.out.println(Arrays.toString(array));
    }
}
