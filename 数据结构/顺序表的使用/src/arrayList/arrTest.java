package arrayList;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.*;

public class arrTest {
    public static ArrayList<Character> func(String str1, String str2) {
        ArrayList<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if (!str2.contains(ch + "")) {
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static List<Character> func2(String str1, String str2) {
        List<Character> arrayList = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            char ch = str1.charAt(i);
            if (!str2.contains(ch + "")) {
                arrayList.add(ch);
            }
        }
        return arrayList;
    }

    public static void main(String[] args) {
        List<Character> ret = func2("welcome to China", "hello");
        //System.out.println(ret);
        for (int i = 0; i < ret.size(); i++) {
            System.out.print(ret.get(i));
        }
    }

    public static void main4(String[] args) {
        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(new Student(10, "小王", 49.9));
        arrayList.add(new Student(50, "小红", 19.9));
        arrayList.add(new Student(10, "大胖", 89.9));
        for (Student s : arrayList) {
            System.out.println(s);
        }
        System.out.println("==========");
        Collections.sort(arrayList);
        for (Student s : arrayList) {
            System.out.println(s);
        }
        //Arrays.sort(arrayList);
    }


    public static void main3(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);
        int size = arrayList1.size();
        for (int i = 0; i < size; i++) {
            System.out.print(arrayList1.get(i) + " ");
        }
        System.out.println();
        for (int x : arrayList1) {
            System.out.print(x + " ");
        }
        System.out.println();

        Iterator<Integer> it = arrayList1.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }
        System.out.println();
        System.out.println("===============");
        ListIterator<Integer> it2 = arrayList1.listIterator();
        while (it2.hasNext()) {
            System.out.print(it2.next() + " ");
        }
    }

    public static void main2(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(3);
        arrayList1.add(4);
        arrayList1.add(5);

        List<Integer> list = arrayList1.subList(1, 3);
        System.out.println(list);//2,3


        System.out.println("======================");
        list.set(0, 99);
        System.out.println(arrayList1);//不变
        System.out.println(list);//99,3

        /*arrayList1.add(0,7);
        System.out.println(arrayList1);

        arrayList1.remove(new Integer(7));
        arrayList1.clear();

        System.out.println(arrayList1);*/

    }

    public static void main1(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        System.out.println(arrayList1);

        ArrayList<Integer> arrayList2 = new ArrayList<>(12);
        arrayList2.add(2);
        arrayList2.add(3);
        arrayList2.add(4);
        System.out.println(arrayList2);

        LinkedList<Integer> arrayList4 = new LinkedList<>();
        ArrayList<Integer> arrayList3 = new ArrayList<>(arrayList4);
    }
}
