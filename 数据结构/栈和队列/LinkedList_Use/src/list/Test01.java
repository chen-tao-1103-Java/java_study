package list;

import java.util.LinkedList;

public class Test01 {
    public static void main(String[] args) {
        LinkedList<String> list1 = new LinkedList<>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        list1.add("d");
        System.out.println(list1);

        LinkedList<String> list2 = new LinkedList<>(list1);
        System.out.println(list2);

        list1.add("e");
        list1.add(0, "hello");
        System.out.println(list1);

        list1.addAll(list2);
        System.out.println(list1);

        String s = list2.get(0);
        System.out.println(s);
        System.out.println(list1.size());

        LinkedList<Integer> list3 = new LinkedList<>();

        //集合为空 NoSuchElementException
        System.out.println(list3.getFirst());


        //集合为空 NoSuchElementException
        System.out.println(list3.remove());

    }
}
