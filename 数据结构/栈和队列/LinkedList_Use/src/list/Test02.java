package list;

import java.sql.SQLOutput;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Test02 {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        list.add("三国演义");
        list.add("水浒传");
        list.add("红楼梦");
        list.add("西游记");


        /**
         * 遍历LinkedList
         */

        //1：使用for循环遍历
        int size = list.size();
        for (int i = 0; i < size; i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();

        //2:使用增强for循环遍历
        for (String srt : list) {
            System.out.print(srt + " ");
        }
        System.out.println();

        //3:使用迭代器正向遍历
        ListIterator<String> it = list.listIterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " ");
        }
        System.out.println();

        //4:使用迭代器反向遍历
        while (it.hasPrevious()) {
            System.out.print(it.previous() + " ");
        }
        System.out.println();



        Iterator<String> it2 = list.iterator();
        while (it2.hasNext()) {
            System.out.println(it2.hasNext() + " ");
        }
        System.out.println();
    }
}
