package list;

import java.util.LinkedList;

public class Test04 {
    public static void main(String[] args) {
        /**
         * 问题描述：由于LinkedList的每个节点除了存储元素本身的数据外，还需要额外存储指向前一个节点和后一个节点的指针,
         * 相比数组实现的列表（如ArrayList）,它占用的内存空间相对较大。如果存储的元素数量较多,且元素对象本身占用内存较大,
         * 可能会导致内存使用过高，影响应用程序的性能和内存管理。
         */


        //解决方法：在选择数据结构时，需要根据元素数量、元素大小以及操作特点综合考虑内存占用情况(应该合理选择数据结构)
        LinkedList<String> largeObjectList = new LinkedList<>();
        for (int i = 0; i < 10000; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 1000; j++) {
                sb.append("a");
            }
            largeObjectList.add(sb.toString());
        }


        /**
         * 问题描述：虽然可以将LinkedList转换为数组（使用toArray方法）,但这个过程可能涉及一定的开销,
         * 尤其是当链表长度较大时。转换过程需要创建一个新的数组,并将链表中的元素逐个复制到数组中，时间复杂度为,
         * 并且可能会占用额外的内存空间（用于存储新创建的数组）。
         */

        LinkedList<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10000; i++) {
            linkedList.add(i);
        }

        // 将LinkedList转换为数组
        Integer[] array = linkedList.toArray(new Integer[0]);
    }
}
