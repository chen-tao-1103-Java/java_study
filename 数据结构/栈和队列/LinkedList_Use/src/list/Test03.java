package list;

import java.util.Iterator;
import java.util.LinkedList;

public class Test03 {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        list.add("三国演义");
        list.add("水浒传");
        list.add("红楼梦");
        list.add("西游记");


        //允许存放null
        list.add(null);
        list.remove(null);
        System.out.println(list);


        /**
         * 并发修改异常：当在遍历LinkedList的同时对其进行结构修改（如添加或删除元素）,
         * 且没有使用迭代器的remove方法时，会抛出ConcurrentModificationException异常。
         * 为了避免这种情况，可以使用迭代器来遍历并修改链表，或者在遍历之前对需要修改的操作进行记录，在遍历结束后统一进行修改。
         *
         * 原因：迭代器在遍历过程中会维护一个内部的修改次数记录，当发现外部通过非迭代器方法修改了链表结构时，
         * 就会认为发生了并发修改，从而抛出异常以保证数据的一致性和避免潜在的错误。如果需要在遍历过程中修改链表结构,
         * 应使用迭代器的remove方法来删除当前迭代的元素,这样可以保证迭代器和链表的修改状态同步，避免异常。
         */


        //错误写法
//        Iterator<String> iterator = list.iterator();
//        while (iterator.hasNext()) {
//            String element = iterator.next();
//            if ("三国演义".equals(element)) {
//                //使用LinkedList的remove方法进行删除,将抛出ConcurrentModificationException异常
//                list.remove();
//            }
//        }

        //正确做法是使用迭代器自带的remove方法进行删除或者添加操作
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            if ("三国演义".equals(element)) {
                iterator.remove();
            }
        }
    }
}
