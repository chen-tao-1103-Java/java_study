/**
 * 单链表实现队列
 */
public class MyQueue {
    public static class ListNode {
        private int val;
        private ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    private ListNode head;//标志链表的头
    private ListNode tail;//标志链表的尾
    private int usedSize;//记录元素的有效个数

    public void offer(int val) {
        ListNode node = new ListNode(val);
        if (head == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = tail.next;
        }
        usedSize++;
    }


    public int poll() {
        if (empty()) {
            return -1;
        }
        int ret = head.val;
        head = head.next;
        if (head == null) {
            tail = null;
        }
        usedSize--;
        return ret;
    }

    public int peek() {
        if (empty()) {
            return -1;
        }
        return head.val;
    }

    public boolean empty() {
        return usedSize == 0;
    }

    public int getUsedSize() {
        return usedSize;
    }
}
