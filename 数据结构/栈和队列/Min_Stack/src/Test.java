import java.util.Stack;

public class Test {
    public static void main(String[] args) {
        MinStack stack = new MinStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        System.out.println(stack.getMin());
        System.out.println(stack.top());
    }
}


