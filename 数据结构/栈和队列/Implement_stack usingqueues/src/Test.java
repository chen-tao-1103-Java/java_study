public class Test {
    public static void main(String[] args) {
        MyStack stack = new MyStack();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        System.out.println(stack.pop());
        System.out.println(stack.top());
        System.out.println(stack.empty());

        while (!stack.empty()) {
            System.out.println(stack.pop());
        }
    }
}
