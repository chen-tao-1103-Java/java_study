public class Test {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);

        int stack1 = myStack.pop();
        System.out.println(stack1);
        stack1 = myStack.pop();
        System.out.println(stack1);
        stack1 = myStack.pop();
        System.out.println(stack1);

        int stack2 = myStack.peek();
        System.out.println(stack2);
        stack2 = myStack.peek();
        System.out.println(stack2);
    }
}
