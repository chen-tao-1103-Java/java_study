public class Dog extends Animal implements IRunning, ISwimming {
    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "正在吃饭");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + "正在睡觉");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + "正在跑");
    }

    @Override
    public void swimming() {
        System.out.println(this.getName() + "正在游泳");
    }

    public void call() {
        System.out.println(this.getName() + "汪汪叫");
    }
}
