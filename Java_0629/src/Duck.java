public class Duck extends Animal implements ISwimming, IFlying, IRunning {
    public Duck(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "正在吃饭");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + "正在睡觉");
    }

    @Override
    public void fly() {
        System.out.println(this.getName() + "正在飞");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + "正在跑");
    }

    @Override
    public void swimming() {
        System.out.println(this.getName() + "正在游泳");
    }

    public void dace() {
        System.out.println(this.getName() + "正在跳舞");
    }
}
