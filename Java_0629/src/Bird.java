public class Bird extends Animal implements IFlying, IRunning {
    public Bird(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "正在吃饭");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + "正在睡觉");
    }

    @Override
    public void fly() {
        System.out.println(this.getName() + "正在飞");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + "正在跑");
    }

    //特有方法
    public void sing() {
        System.out.println(this.getName() + "唱歌");
    }
}
