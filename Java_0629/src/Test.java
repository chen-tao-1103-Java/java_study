public class Test {

    //向上转型
    public static void fun1(Animal animal) {
        animal.eat();
        animal.sleep();
    }

    //向下转型
    public static void fun2(Animal animal) {
        switch (animal) {
            case Dog dog -> {
                dog.call();
                dog.swimming();
            }
            case Cat cat -> {
                cat.catchMouse();
                cat.run();
            }
            case Bird bird -> {
                bird.sing();
                bird.fly();
            }
            default -> {
                Duck duck = (Duck) animal;
                duck.dace();
                duck.fly();
            }
        }
    }

    public static void main(String[] args) {
        fun1(new Bird("自由哥", 2));
        fun1(new Cat("tom", 4));
        fun1(new Dog("小黑", 3));
        fun1(new Duck("丑小鸭", 5));
        System.out.println("===============");


        fun2(new Bird("自由哥", 12));
        fun2(new Dog("小黑", 3));
    }
}
