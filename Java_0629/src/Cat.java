public class Cat extends Animal implements IRunning {
    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "正在吃饭");
    }

    @Override
    public void sleep() {
        System.out.println(this.getName() + "正在睡觉");
    }

    @Override
    public void run() {
        System.out.println(this.getName() + "正在跑");
    }

    public void catchMouse() {
        System.out.println(this.getName() + "抓老鼠");
    }
}
