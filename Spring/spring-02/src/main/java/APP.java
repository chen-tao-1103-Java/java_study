import com.java.demo.*;
import com.java.demo.service.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class APP {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

        StudentController studentController =
                context.getBean("studentController", StudentController.class);

        studentController.sayHello();


        ApplicationContext context1 =
                new ClassPathXmlApplicationContext("spring-config.xml");

        SController sController = context1.getBean("SController", SController.class);
        sController.sayHi();


        new ClassPathXmlApplicationContext("spring-config.xml")
                .getBean("studentComponent", StudentComponent.class)
                .sayHi();

        new ClassPathXmlApplicationContext("spring-config.xml")
                .getBean("studentService", StudentService.class)
                .sayHi();


        new ClassPathXmlApplicationContext("spring-config.xml")
                .getBean("studentRepository", StudentRepository.class)
                .sayHi();

        new ClassPathXmlApplicationContext("spring-config.xml")
                .getBean("studentConfiguration", StudentConfiguration.class)
                .sayHi();

        new ClassPathXmlApplicationContext("spring-config.xml")
                .getBean("userController", UserController.class)
                .sayHi();
    }
}
