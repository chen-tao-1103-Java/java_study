package com.java.demo;

import org.springframework.stereotype.Repository;

@Repository
public class StudentRepository {
    public void sayHi() {
        System.out.println("StudentRepository Hi");
    }
}
