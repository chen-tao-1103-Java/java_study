package com.java.demo;

import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.stereotype.Controller;

@Controller
public class SController {
    public void sayHi(){
        System.out.println("SController sayHi()");
    }
}
