package com.java.demo;

import org.springframework.stereotype.Service;

@Service
public class StudentService {
    public void sayHi() {
        System.out.println("StudentService Hi");
    }
}
