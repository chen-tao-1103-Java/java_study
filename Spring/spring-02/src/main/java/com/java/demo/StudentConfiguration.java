package com.java.demo;

import org.springframework.context.annotation.Configuration;

@Configuration
public class StudentConfiguration {
    public void sayHi(){
        System.out.println("StudentConfiguration Hi");
    }
}
