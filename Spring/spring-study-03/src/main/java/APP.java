import com.java.demo.controller.UserController;
import com.java.demo.entity.User;
import com.java.demo.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class APP {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

        User user = context.getBean("u1", User.class);

        System.out.println(user.toString());

        System.out.println("=============");
//
//        UserController userController
//                = context.getBean("userController", UserController.class);
//        userController.sayHi();


        UserService userService =
                context.getBean("userService", UserService.class);
        userService.sayHi();
        System.out.println(userService.age);
    }
}
