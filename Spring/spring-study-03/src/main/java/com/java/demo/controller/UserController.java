package com.java.demo.controller;

import com.java.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

//    @Autowired
//    //1.属性注入
//    private UserService userService;


    //2.setter注入
//    private UserService userService;
//
//    @Autowired
//    public void setUserService(UserService userService) {
//        this.userService = userService;
//    }
//


    //3.构造方法注入
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
        userService.age = 20;
    }


    public void sayHi() {
        System.out.println("UserController Hi");
        userService.sayHi();
        userService.age = 20;
    }
}
