package com.java.demo.service;

import org.springframework.stereotype.Service;

@Service
public class UserService {
    public Integer age = 10;

    public void sayHi() {
        System.out.println("Hi UserService");
    }
}
