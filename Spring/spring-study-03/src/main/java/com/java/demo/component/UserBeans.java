package com.java.demo.component;

import com.java.demo.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserBeans {
    @Bean(name = {"u1","u2"})//Bean重命名
    public User user1() {
        User user = new User();
        user.setUsername("张三");
        user.setAge(18);
        user.setUid(1);
        return user;
    }
}
