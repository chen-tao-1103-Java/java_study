import com.spring.demo.Student;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class APP2 {
    public static void main(String[] args) {
        //使用beanFactory来获取上下文对象
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        Student student = beanFactory.getBean("student", Student.class);
        student.sayHello();
    }
}
