import com.spring.demo.Student;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class APP {
    public static void main(String[] args) {
//        //1.创建Spring上下文对象
//        ApplicationContext context =
//                new ClassPathXmlApplicationContext("spring-config.xml");
//
//        //2.获取bean对象
//        Student student = context.getBean("student", Student.class);
//
//        //3.使用对象
//        student.sayHello();

//        new ClassPathXmlApplicationContext("spring-config.xml")
//                .getBean("student", Student.class).sayHello();
//

        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

        Student student = context.getBean("student", Student.class);
        student.sayHello();
    }
}
