import java.util.Arrays;

class Sort {

/*private static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;

            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }


    private static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            //第n趟结束之后,进行交换
            if (minIndex != i) {//防止有序的情况,minIndex和i就在同一位置,此时不用交换
                swap(array, minIndex, i);
            }
        }
    }

    private static void swap(int[] array, int minIndex, int i) {
        int tmp = array[minIndex];
        array[minIndex] = array[i];
        array[i] = tmp;
    }*/

    private static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int k = i - 1;
            for (; k >= 0; k--) {
                if (array[k] > tmp) {
                    array[k + 1] = array[k];
                } else {
                    break;
                }
            }
            array[k + 1] = tmp;
        }
    }

    private static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            //进行交换
            if (minIndex != i) {
                swap(array, minIndex, i);
            }
        }
    }

    private static void swap(int[] array, int minIndex, int i) {
        int tmp = array[minIndex];
        array[minIndex] = array[i];
        array[i] = tmp;
    }

    private static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int minIndex = left;
            int maxIndex = left;
            for (int i = left + 1; i <= right; i++) {
                if (array[i] < array[minIndex]) {
                    minIndex = i;
                }
                if (array[i] > array[maxIndex]) {
                    maxIndex = i;
                }
            }
            //交换
            swap(array, minIndex, left);
            //没懂
            //如果max下标
            if (maxIndex == left) {
                maxIndex = minIndex;
            }
            swap(array, maxIndex, right);
            left++;
            right--;
        }
    }

    public static void main(String[] args) {
        int[] array = {5, 1, 3, 28, 9, 2};
        insertSort(array);
        System.out.println(Arrays.toString(array));
        selectSort2(array);
        System.out.println(Arrays.toString(array));
    }
}
