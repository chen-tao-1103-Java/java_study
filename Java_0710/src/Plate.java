public class Plate<T> { // 设置泛型 shift+f6
    private T plate;

    public T getPlate() {
        return plate;
    }

    public void setPlate(T plate) {
        this.plate = plate;
    }
}
