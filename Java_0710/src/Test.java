public class Test {
    public static void fun(Plate<? super Fruit> temp) {
        temp.setPlate(new Apple());
        temp.setPlate(new Banana());
        temp.setPlate(new Fruit());
        //不能存放Fruit的父类

        //Fruit fruit = temp.getPlate();  不能取数据 因为无法知道取出的数据类型是什么
    }

    public static void main(String[] args) {
        Plate<Fruit> plate1 = new Plate<>();
        plate1.setPlate(new Fruit());
        fun(plate1);

        Plate<Food> plate2 = new Plate<>();
        plate2.setPlate(new Food());
        fun(plate2);
    }

    public static void main1(String[] args) {
        Plate<Apple> plate1 = new Plate<>();
        plate1.setPlate(new Apple());
        fun1(plate1);

        Plate<Banana> plate2 = new Plate<>();
        plate2.setPlate(new Banana());
        fun1(plate2);
    }

    public static void fun1(Plate<? extends Fruit> temp) {
        /*
        不能放东西
        temp.setPlate(new Apple());
        temp.setPlate(new Banana());
        temp.setPlate(new Fruit());
        */
        Fruit fruit = temp.getPlate();

    }
}
