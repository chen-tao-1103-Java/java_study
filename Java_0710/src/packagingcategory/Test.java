package packagingcategory;

public class Test {

    public static void main3(String[] args) {
        Integer a1 = 100;
        Integer a2 = 100;
        System.out.println(a1 == a2);

        Integer a3 = 200;
        Integer a4 = 200;
        System.out.println(a3 == a4);
    }

    public static void main2(String[] args) {
        Integer a = 20;
        int b = a;//自动拆箱
        float f = a.floatValue();
        long l = a.longValue();
        System.out.println(b);
        System.out.println(f);
        System.out.println(l);
    }

    public static void main1(String[] args) {
        int a = 10;
//        Integer b = new Integer(10);
        Integer v = 20;
        Integer c = Integer.valueOf(100);
    }
}
