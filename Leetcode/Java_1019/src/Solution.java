public class Solution {
    public boolean findTargetIn2DPlants(int[][] plants, int target) {
        if (plants == null || plants.length == 0) {
            return false;
        }
        int row = 0;
        int col = plants[row].length - 1;
        while (row < plants.length && col >= 0) {
            if (target < plants[row][col]) {
                col--;
            } else if (target > plants[row][col]) {
                row++;
            } else {
                return true;
            }
        }
        return false;
    }
}
