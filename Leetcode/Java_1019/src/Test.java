public class Test {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[][] array = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println(solution.findTargetIn2DPlants(array, 3));
    }
}
