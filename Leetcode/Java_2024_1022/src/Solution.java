public class Solution {
    public int removeDuplicates(int[] nums) {
        //边界条件
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int slow = 0;
        int fast = 1;
        while (fast < nums.length) {
            if (nums[fast] != nums[slow]) {
                nums[++slow] = nums[fast];
            } else {
                fast++;
            }
        }
        return slow + 1;
    }
}

