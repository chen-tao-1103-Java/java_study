public class Test {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] arr = {1, 1, 2};
        System.out.println(solution.removeDuplicates(arr));

        int[] arr2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        System.out.println(solution.removeDuplicates(arr2));
    }
}
