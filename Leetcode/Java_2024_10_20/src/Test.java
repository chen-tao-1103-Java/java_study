import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        /**
         * 测试用例
         * 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
         * 输出：[1,2,2,3,5,6]
         *
         * 输入：nums1 = [1], m = 1, nums2 = [], n = 0
         * 输出：[1]
         *
         * 输入：nums1 = [0], m = 0, nums2 = [1], n = 1
         * 输出：[1]
         */

        int[] nums1 = new int[7];
        nums1[0] = 1;
        nums1[1] = 2;
        nums1[2] = 3;
        nums1[3] = 20;
        int[] nums2 = {2, 5, 6};
        Solution solution = new Solution();
        solution.merge(nums1, 4, nums2, 3);
        System.out.println(Arrays.toString(nums1));
    }
}
