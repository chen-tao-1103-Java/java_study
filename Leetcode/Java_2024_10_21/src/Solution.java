public class Solution {
    public boolean isPalindrome(String s) {
        // if(s.length() == 0 || s.trim().length() == 0) return true;
        int left = 0;
        int right = s.length() - 1;
        while (left <= right) {

            // Character.isLetterOrDigit用于判断是否是字母或者数字
            while (left <= right && !Character.isLetterOrDigit(s.charAt(left))) {
                left++;
            }
            while (left <=right && !Character.isLetterOrDigit(s.charAt(right))) {
                right--;
            }
            if (Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right))) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
