package exercise;

public class SinglyLinkedList {
    //节点内部类
    public static class ListNode {
        ListNode next;
        int val;

        public ListNode(int val) {
            this.val = val;
        }

        @Override
        public String toString() {
            return "val = " + val;
        }
    }

    public ListNode head;

    public void createListNode() {
        ListNode listNode1 = new ListNode(20);
        ListNode listNode2 = new ListNode(30);
        ListNode listNode3 = new ListNode(30);
        ListNode listNode4 = new ListNode(45);
        ListNode listNode5 = new ListNode(30);
        ListNode listNode6 = new ListNode(10);
        listNode1.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        listNode5.next = listNode6;
        this.head = listNode1;
    }

    /**
     * @param head：表示单链表的头节点
     * @param val：表示需要删除的节点的值
     * @return删除后的链表的头节点
     */
    public ListNode deleteNode(ListNode head, int val) {
        //链表为空
        if (head == null) {
            return null;
        }

        ListNode cur = head.next;
        ListNode prev = head;
        while (cur != null) {
            if (cur.val == val) {
                prev.next = cur.next;
            } else {
                prev = cur;
            }
            cur = cur.next;
        }
        //头节点满足删除条件
        if (head.val == val) {
            head = head.next;
        }
        return head;
    }
}
