package genericity;

public class Test {

    public static void main(String[] args) {
        Message<Integer> message = new Message<>();
        message.setMessage(10);
        fun(message);
    }

    public static void main7(String[] args) {
        Message<String> message = new Message<>();
        message.setMessage("一起学习java");
        fun(message);
    }

    public static void fun(Message<?> temp) {
        System.out.println(temp.getMessage());
        //temp.setMessage(10);
    }


    public static void main6(String[] args) {
        Student<Integer> student1 = new Student<>();
        Student<String> student2 = new Student<>();
        System.out.println(student1);
        System.out.println(student2);
    }


    public static void main5(String[] args) {
        Alg3 alg3 = new Alg3();
        Integer[] array = {1, 2, 3, 4};

        Integer ret = alg3.<Integer>findMax(array);
        System.out.println(ret);
    }

    public static void main4(String[] args) {
        Integer[] array = {1, 2, 3, 4};
        Integer ret = Alg2.<Integer>findMax(array);
        System.out.println(ret);
    }

    public static void main3(String[] args) {
        Alg<Integer> alg = new Alg<>();
        Integer[] array = {1, 2, 3, 4};
        Integer ret = alg.findMax(array);
        System.out.println(ret);
    }

    public static void main2(String[] args) {
        Alg<Person> alg = new Alg<>();
        Person[] people = {new Person(10), new Person(15)};
        Person person = alg.findMax(people);
        System.out.println(person);
    }

    public static void main1(String[] args) {
        int[] array = {1, 2, 3, 4};
        System.out.println(array[0] > array[1]);
    }
}
