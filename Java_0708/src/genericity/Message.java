package genericity;

public class Message<T> {
    private T message;//消息

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }
}
