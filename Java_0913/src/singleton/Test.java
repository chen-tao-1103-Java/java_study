package singleton;

/**
 * 饿汉模式
 */
class Singleton {
    //创建实例
    private static Singleton instance = new Singleton();

    //构造方法
    private Singleton() {

    }

    //获取实例
    public static Singleton getInstance() {
        return instance;
    }
}


public class Test {
    public static void main(String[] args) {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println(s1 == s2);
//         new Singleton();
    }
}

