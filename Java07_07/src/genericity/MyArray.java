package genericity;

import java.lang.reflect.Array;

public class MyArray<T> {
    //public T[] obj2 = new T[10];
    public T[] obj;

    public MyArray() {
    }


    public MyArray(Class<T> clazz, int capacity) {
        obj = (T[]) Array.newInstance(clazz, capacity);
    }

    public void setVal(int pos, T val) {
        obj[pos] = val;
    }

    public T getPos(int pos) {
        return obj[pos];
    }

    public T[] getObj2() {
        return obj;
    }
}
