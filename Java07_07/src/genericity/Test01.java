package genericity;

public class Test01 {

    public static void main(String[] args) {
        MyArray<Integer> myArray = new MyArray<>(Integer.class, 10);
        Integer[] tmp = myArray.getObj2();

    }


    public static void main3(String[] args) {
        MyArray myArray = new MyArray();
        myArray.setVal(0, 10);
        myArray.setVal(1, 2);
        myArray.setVal(2, 6);
        myArray.setVal(3, "hell");
        int a = (int) myArray.getPos(1);
    }

    public static void main2(String[] args) {
        MyArray<Integer> myArray = new MyArray<>();
        myArray.setVal(0, 10);
        myArray.setVal(1, 2);
        myArray.setVal(2, 6);
        int a = myArray.getPos(1);
        MyArray<String> myArray2 = new MyArray<>();
        myArray2.setVal(0, "hello");
        myArray2.setVal(1, "hello2");
        String str = myArray2.getPos(0);
    }


    public static void main1(String[] args) {
        //JVM会做一些改变 把1 2 3 4 这些数据放到这个数组当中
        //Object[] array = {1,2,3,4,"hello","ok"};
        //String[] arr2 = (Object)array;
        String[] array3 = (String[]) new Object[10];
    }
}


