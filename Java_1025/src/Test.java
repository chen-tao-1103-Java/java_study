public class Test {
    public static void main(String[] args) {
        MySingleList list = new MySingleList();
        list.createList();

        //遍历链表
        list.display();

        //获取链表长度
        int size = list.size();
        System.out.println(size);

        //链表中是否包含12
        boolean ret = list.contains(12);
        System.out.println(ret);

        //头插
        list.addFirst(100);

        //尾插
        list.addLast(200);
        list.display();

        //任意位置插入
        list.addIndex(4, 100);
        list.display();

        //清空链表
        list.display();
    }
}
