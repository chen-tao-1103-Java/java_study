package thread;

public class ThreadTest01 {
    public static void main(String[] args) {
        //哲学家就餐问题解决
        // 假设 locker1 是 1 号, locker2 是 2 号, 约定先拿小的, 后拿大的.
        Object locker1 = new Object();
        Object locker2 = new Object();

        Thread t1 = new Thread(() -> {
            synchronized (locker1) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (locker2) {
                    System.out.println("t1获取两把锁");
                }
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (locker1) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (locker2) {
                    System.out.println("t2获取两把锁");
                }
            }
        });
        t1.start();
        t2.start();
    }
}
