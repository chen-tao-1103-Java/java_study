import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            int n = scanner.nextInt();
            System.out.println(n);
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("输入的数据不匹配");
        } finally {
            System.out.println("执行了finally部分，一般用来关闭资源！");
        }
    }

    public static void test2(int a) {
        try {
            if (a == 10) {
                throw new CloneNotSupportedException();//受查异常  必须此时处理
            }
            System.out.println("1");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("11111");
    }

    //此时其实不算处理异常  声明异常
    public static void test22(int a) throws CloneNotSupportedException, ArithmeticException, NullPointerException {
        if (a == 10) {
            throw new CloneNotSupportedException();//受查异常  必须此时处理
        }
        System.out.println("11111");
    }

    public static void main13(String[] args) {
        try {
            test22(10);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("2");


        /*int x = 10;
        if(x == 10) {
            throw new Exception();//默认是受查异常
        }*/
    }


    public static void test1(int a) {
        try {
            if (a == 10) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        System.out.println("1");
    }


    public static void main12(String[] args) {
        try {
            test1(10);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        System.out.println("haha");
    }

    public static void main11(String[] args) {
        try {
            System.out.println(10 / 0);
            int[] array = {1, 2, 3, 4};
            array = null;
            System.out.println(array[10]);
        } catch (ArithmeticException e) {
            e.printStackTrace();//快速的定位异常出现的位置
            System.out.println("你这里出现算术异常了！");
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕捉到了空指针异常！");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println("数组越界异常！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("执行后续代码！");
    }

    public static void main10(String[] args) {
        try {
            System.out.println(10 / 0);
            int[] array = {1, 2, 3, 4};
            array = null;
            System.out.println(array[10]);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();//快速的定位异常出现的位置
            System.out.println("你这里出现异常了！");
        }
        System.out.println("执行后续代码！");
    }

    public static void main9(String[] args) {
        try {
            System.out.println(10 / 0);
            int[] array = {1, 2, 3, 4};
            array = null;
            System.out.println(array[10]);
        } catch (Exception e) {
            e.printStackTrace();//快速的定位异常出现的位置
            System.out.println("你这里出现异常了！");
        }
        System.out.println("执行后续代码！");
    }

    public static void main8(String[] args) {
        try {
            System.out.println(10 / 0);
            int[] array = {1, 2, 3, 4};
            array = null;
            System.out.println(array[10]);
        } catch (ArithmeticException e) {
            e.printStackTrace();//快速的定位异常出现的位置
            System.out.println("你这里出现算术异常了！");
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕捉到了空指针异常！");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println("数组越界异常！");
        }
        System.out.println("执行后续代码！");
    }

    public static void main7(String[] args) {
        try {
            //System.out.println(10/0);
            int[] array = {1, 2, 3, 4};
            //array = null;
            System.out.println(array[10]);
        } catch (ArithmeticException | NullPointerException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();//快速的定位异常出现的位置
            //System.out.println("你这里出现异常了！");
        }

        System.out.println("执行后续代码！");
    }

    public static void main6(String[] args) {
        try {
            System.out.println(10 / 0);
            int[] array = {1, 2, 3, 4};
            array = null;
            System.out.println(array[10]);
        } catch (ArithmeticException e) {
            e.printStackTrace();//快速的定位异常出现的位置
            System.out.println("你这里出现算术异常了！");
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕捉到了空指针异常！");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println("数组越界异常！");
        }
        System.out.println("执行后续代码！");
    }

    public static void func() {
        func();
    }

    public static void main4(String[] args) {
        func();
    }


    public static void main2(String[] args) {
        //System.out.println(10/0);

        /*int[] arr = {1, 2, 3};
        System.out.println(arr[100]);*/

       /* int[] arr = null;
        System.out.println(arr.length);*/

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(n);

    }

    public static int countSegments(String s) {
        s = s.trim();
        if (s == null || s.length() == 0) {
            return 0;
        }

        String[] strings = s.split(" ");
        int count = 0;
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].length() != 0) {
                count++;
            }
        }
        return count;
    }

    public String toLowerCase(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch >= 'A' && ch <= 'Z') {
                sb.append((char) (ch + 32));
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static void main1(String[] args) {
        countSegments(", , , ,        a, eaefa");
        /*String s = "        ";
        String[] ret = s.split(" ");
        System.out.println(ret);*/
    }
}
