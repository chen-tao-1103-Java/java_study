public class Test {
    public static void main(String[] args) {

        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);

        int x1 = myStack.pop();
        System.out.println(x1);
        x1 = myStack.pop();
        System.out.println(x1);
        x1 = myStack.pop();
        System.out.println(x1);

        int x2 = myStack.peek();
        System.out.println(x2);
        x2 = myStack.peek();
        System.out.println(x2);
    }
}
