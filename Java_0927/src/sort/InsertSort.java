package sort;

import java.util.Arrays;

public class InsertSort {
    /**
     * 时间复杂度：
     * 最坏情况下：逆序：O(N^2)
     * 最好情况下：什么情况下是最好情况？ 有序：O(n)
     * 空间复杂度：  O(1)
     * 稳定性： 稳定
     * 当数据量不多，且基本上趋于有序的时候，直接插入排序是非常快的
     *
     * @param array
     */
    public static void insertSort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int tmp = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (array[j] > tmp) {
                    array[j + 1] = array[j];
                } else {
                    //array[j+1] = tmp;
                    break;
                }
            }
            array[j + 1] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] array = {2, 9, 4, 8, 1};
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }
}
