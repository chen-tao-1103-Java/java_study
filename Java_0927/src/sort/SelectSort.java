package sort;

import java.util.Arrays;

public class SelectSort {
    /**
     * 选择排序
     * 时间复杂度：O(n^2)   和数据是否有序无序 无关 都是这样的
     * 空间复杂度：O(1)
     * 稳定性：不稳定
     *
     * @param array
     */
    public static void selectSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    //更新minIndex的值
                    minIndex = j;
                }
            }
            //处理两个下标是一样的情况
            if (i != minIndex) {
                swap(array, minIndex, i);
            }
        }
    }

    private static void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    public static void selectSort2(int[] array) {
        int left = 0;
        int right = array.length - 1;
        while (left < right) {
            int minIndex = left;
            int maxIndex = left;
            for (int j = left + 1; j <= right; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
                if (array[j] > array[maxIndex]) {
                    maxIndex = j;
                }
            }
            //把最小值交换到前面
            swap(array, minIndex, left);
            //这里 如果max下标正好是Left说明上次 已经把最大值从Left位置换到了minIdex位置
            if (maxIndex == left) {
                maxIndex = minIndex;
            }
            //把最大值交换到后面
            swap(array, maxIndex, right);
            left++;
            right--;
        }
    }

    public static void main(String[] args) {
        int[] array = {2, 9, 4, 8, 1};
        selectSort(array);
        System.out.println(Arrays.toString(array));
    }
}
