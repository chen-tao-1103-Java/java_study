package map_set;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

class Student implements Comparator<Student> {
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {

    }

    @Override
    public String toString() {
        return "name=" + name + " " + "age=" + age + "  " + "value:" + " ";
    }

    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}


public class Test3 {
    public static void main(String[] args) {
        //1：在创建TreeMap对象时传入一个实现了Comparator接口的对象
        TreeMap<Student, Integer> map = new TreeMap<>(new Student());

        map.put(new Student("李四", 12), 1);
        map.put(new Student("张三", 18), 1);
        map.put(new Student("王五", 20), 1);

        for (Map.Entry<Student, Integer> entry : map.entrySet()) {
            System.out.println("key:" + entry.getKey() + " -> " + entry.getValue());
        }
        System.out.println("=======================");

        //2：在创建TreeMap的时候传入一个实现Comparator接口的匿名内部类
        TreeMap<Student, Integer> map2 = new TreeMap<>(new Comparator<Student>() {

            @Override
            public int compare(Student o1, Student o2) {
                return o1.age - o2.age;
            }
        });
        map2.put(new Student("张三", 12), 1);
        map2.put(new Student("李四", 14), 1);
        map2.put(new Student("王五", 2), 1);
        for (Map.Entry<Student, Integer> entry : map2.entrySet()) {
            System.out.println("key: " + entry.getKey() + entry.getValue());
        }
        System.out.println("=========================");

        //3.使用lambda表达式
        TreeMap<Student, Integer> map3 = new TreeMap<>((o1, o2) -> o1.age - o2.age);
        map3.put(new Student("张飞", 12), 1);
        map3.put(new Student("刘备", 14), 1);
        map3.put(new Student("关羽", 2), 1);
        for (Map.Entry<Student, Integer> entry : map3.entrySet()) {
            System.out.println("key: " + entry.getKey() + entry.getValue());
        }
    }
}

