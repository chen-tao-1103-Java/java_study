package map_set;

import java.util.TreeMap;

public class Test01 {
    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap<>();//无参构造

        //添加元素
        map.put("三国演义", 1);
        map.put("水浒传", 2);
        map.put("红楼梦", 3);
        map.put("西游记", 4);
// NPE       map.put(null,1);
        System.out.println(map);

        //获取元素
        System.out.println(map.get("西游记"));
        System.out.println(map.get("h"));//如果没有对应的key,那么返回null

        //获取第一个键
        System.out.println(map.firstKey());

        //获取最后一个键
        System.out.println(map.lastKey());

        //是否包含指定的键
        System.out.println(map.containsKey("西游记"));

        //是否包含指定的值
        System.out.println(map.containsValue(9));

        //根据键,删除键值对
        System.out.println(map.remove("西游记"));
        System.out.println(map);

        System.out.println(map.remove("红楼梦", 3));

        //获取长度
        System.out.println(map.size());

    }
}
