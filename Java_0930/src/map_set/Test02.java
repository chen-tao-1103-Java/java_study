package map_set;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Test02 {
    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap<>();
        map.put("三国演义", 1);
        map.put("水浒传", 2);
        map.put("红楼梦", 3);
        map.put("西游记", 4);
        // System.out.println(map);


        /**
         * TreeMap的遍历方式
         * 1:使用键集(keySet)遍历
         * 2:使用值集(values)遍历
         * 3:使用键值对集(entrySet)遍历
         * 4:使用迭代器遍历键集
         * 5:使用迭代器遍历值集
         */

        //1：使用键集(keySet)遍历
        for (
                String key : map.keySet()) {
            //通过键可以找到值
            Integer value = map.get(key);
            System.out.print(key + "->" + value + "   ");
        }
        System.out.println();

        //2：使用值集(values)遍历
        for (
                Integer value : map.values()) {
            //通过值无法找到键
            System.out.print(value + " ");
        }
        System.out.println();

        //3：使用键值对集(entrySet)遍历
        for (
                Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
        }
        System.out.println();

        //4.使用迭代器遍历键集
        Iterator<String> keyIterator = map.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            Integer value = map.get(key);
            System.out.print(key + "->" + value + "  ");
        }
        System.out.println();

        //5.使用迭代器遍历值集
        Iterator<Integer> valueIterator = map.values().iterator();
        while (valueIterator.hasNext()) {
            Integer value = valueIterator.next();
            System.out.print(value + " ");
        }
        System.out.println();
    }
}


