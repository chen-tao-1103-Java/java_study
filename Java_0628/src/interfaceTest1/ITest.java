package interfaceTest1;

public interface ITest {
    int size = 10;//public static final

    void draw();//public abstract

    default public void func() {    //默认方法不需要重写
        System.out.println("默认方法！");
    }

    public static void func2() {       //静态成员方法不需要重写
        System.out.println("你好");
    }
}
