package list;

import java.util.Arrays;

public class MyArrayList {
    private int[] elem;
    private int usedSize;
    private static final int DEFAULT_CAPACITY = 10;

    public MyArrayList() {
        elem = new int[DEFAULT_CAPACITY];
    }

    public int getUsedSize() {
        return usedSize;
    }

    //打印数组
    public void display() {
        for (int i = 0; i < usedSize; i++) {
            System.out.print(elem[i] + " ");
        }
        System.out.println();
    }

    //添加元素,默认添加到数组末尾
    public void add(int val) {
        //数组满了,需要扩容
        if (isFull()) {
            elem = Arrays.copyOf(elem, 2 * elem.length);
        }
        elem[usedSize++] = val;
    }

    //判断数组是否满了
    public boolean isFull() {
        return elem.length == usedSize;
    }

    /**
     * @param pos 表示需要插入的位置
     * @param val 表示需要插入的值
     */
    public void add(int pos, int val) {
        //1：满了,扩容
        if (isFull()) {
            elem = Arrays.copyOf(elem, 2 * elem.length);
        }
        //2:pos位置的合法性判断
        if (pos < 0 || pos > usedSize) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        //3:挪动pos位置及其以后的数据
        for (int i = usedSize - 1; i >= pos; i--) {
            elem[i + 1] = elem[i];
        }
        //4:走到这里数据已经挪完了,开始插入数据
        elem[pos] = val;
        usedSize++;
    }

    // 判定是否包含某个元素
    public boolean contains(int val) {
        for (int i = 0; i < usedSize; i++) {
            if (elem[i] == val) {
                return true;
            }
        }
        return false;
    }

    // 查找某个元素对应的位置,没有找到返回-1
    public int IndexFindVal(int val) {
        for (int i = 0; i < usedSize; i++) {
            if (elem[i] == val) {
                return i;
            }
        }
        return -1;
    }

    // 获取 pos 位置的元素
    public int getVal(int pos) {
        //1:数组为空
        if (isEmpty()) {
            System.out.println("List is empty");
        }
        //2：判断pos位置合法性
        if (pos < 0 || pos >= usedSize) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        return elem[pos];
    }

    //判断数组是否为空
    public boolean isEmpty() {
        return usedSize == 0;
    }


    // 给 pos 位置的元素 更新 为 value
    public void set(int pos, int val) {
        //1:空表
        if (isEmpty()) {
            System.out.println("List is empty");
        }
        //2:pos位置合法性判断
        if (pos < 0 || pos >= usedSize) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        //3:修改操作
        elem[pos] = val;
    }

    //获取表长度
    public int size() {
        return usedSize;
    }

    //删除第一次出现的关键字key
    public void remove(int val) {
        //1:空表
        if (isFull()) {
            System.out.println("List is empty");
        }
        //2:找到val对应的下标
        int index = IndexFindVal(val);
        if (index == -1) {
            System.out.println("没有您要找的数字");
            return;
        }

        // i <= size() - 1发生越界
        for (int i = index; i < size() - 1; i++) {
            elem[i] = elem[i + 1];
        }
        usedSize--;
    }

    // 清空顺序表
    public void clear() {
        /*for (int i = 0; i < size(); i++) {
            this.elem[i] = null;
        }
        this.usedSize=0;*/
        this.usedSize = 0;
    }
}
