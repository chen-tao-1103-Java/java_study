package list;

public class PosIllegalException extends RuntimeException {
    public PosIllegalException() {

    }

    public PosIllegalException(String message) {
        super(message);
    }
}
