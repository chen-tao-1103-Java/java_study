package map_set;

import java.util.ArrayList;
import java.util.TreeSet;

public class Test3 {
    public static void main(String[] args) {
        //创建TreeSet对象
        TreeSet<Integer> set = new TreeSet<>();
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(40);
        // set.add(null); 键不能为空NPE

        ArrayList<Integer> list = new ArrayList<>();
        list.add(100);
        list.add(200);

        //打印set
        System.out.println(set);

        //检查元素是否存在,返回一个布尔值
        System.out.println(set.contains(10));

        //返回元素个数
        System.out.println(set.size());

        //是否为空
        System.out.println(set.isEmpty());

        //删除元素
        System.out.println(set.remove(40));
        System.out.println(set);

        //键不能为空NPE
        System.out.println(set);

        //清空操作
        set.clear();
        System.out.println(set);

        //添加所有
        System.out.println(set.addAll(list));
        System.out.println(set);

    }
}

