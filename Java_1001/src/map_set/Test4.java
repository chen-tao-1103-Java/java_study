package map_set;

import java.util.Iterator;
import java.util.TreeSet;

public class Test4 {
    public static void main(String[] args) {
        //1:增强for循环遍历
        TreeSet<String> set = new TreeSet<>();
        set.add("A");
        set.add("B");
        set.add("C");
        set.add("D");
        for (String str : set) {
            System.out.print(str + " ");
        }
        System.out.println();

        //2:使用迭代器Iterator
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        //3:StreamAPI
        set.stream().forEach(System.out::print);
    }
}

