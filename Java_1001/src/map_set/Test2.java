package map_set;

import java.util.HashMap;
import java.util.Map;

public class Test2 {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("三国演义", 1);
        map.put("水浒传", 2);
        map.put("西游记", 3);
        map.put("红楼梦", 4);
        map.put(null, null);
        map.put(null, 1);

        //1:使用增强for循环遍历键集
        for (String key : map.keySet()) {
            System.out.println(key + "->" + map.get(key));
        }
        System.out.println();

        //2：使用键值对遍历
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }
    }
}

