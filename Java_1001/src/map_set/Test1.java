package map_set;

import java.util.HashMap;

public class Test1 {
    public static void main(String[] args) {
        //创建HashMap对象
        HashMap<String, Integer> map = new HashMap<>();
        map.put("三国演义", 1);
        map.put("水浒传", 2);
        map.put("西游记", 3);
        map.put("红楼梦", 4);
        System.out.println(map);

        //获取key,返回对应的值
        int val = map.get("西游记");
        System.out.println(val);

        //检查key是否存在
        System.out.println(map.containsKey("西游记"));
    }
}
