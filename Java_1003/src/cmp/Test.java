package cmp;

import java.util.Comparator;
import java.util.TreeMap;

class Student implements Comparator<Student> {

    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public int compare(Student o1, Student o2) {
        return o1.getAge() - o2.getAge();
    }

    @Override
    public String toString() {
        return "key:" + "name=" + name + " " + "age=" + age + " " + "value:";
    }
}

public class Test {
    public static void main(String[] args) {
        TreeMap<Student, Integer> map = new TreeMap<>(new Student());
        map.put(new Student("John", 20), 10);
        map.put(new Student("Jane", 21), 20);
        map.put(new Student("Jack", 22), 30);
        System.out.println(map);
    }
}
