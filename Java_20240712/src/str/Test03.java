package str;

public class Test03 {
    public static void main(String[] args) {

    }

    public boolean isPalindrome(String s) {
        s = s.toLowerCase();//全部整体转为小写字符

        int left = 0;
        int right = s.length() - 1;
        while (left < right) {

            while (left < right && !isEffective(s.charAt(left))) {
                left++;
            }
            //left下标一定是 有效的字符
            while (left < right && !isEffective(s.charAt(right))) {
                right--;
            }
            //right下标一定是 有效的字符
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    public boolean isEffective(char ch) {
        if (Character.isLetter(ch) || Character.isDigit(ch)) {
            return true;
        }
        return false;
        //return Character.isLetterOrDigit(ch);//是不是数字字符或者字母
    }

    public int firstUniqChar(String s) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            count[ch - 'a']++;
        }

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (count[ch - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }
}
