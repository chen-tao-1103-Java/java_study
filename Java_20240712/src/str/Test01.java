package str;

public class Test01 {
    public static void main(String[] args) {
        String str = "helloabc";
        System.out.println(str.contains("abc"));

        String str1 = "abcd";
        String str2 = "abcd";
        String str3 = new String("abcd");
        String str4 = new String("abcd");

        System.out.println(str1 == str2);
        System.out.println(str3 == str4);
        System.out.println(str1 == str3);

        String str5 = "abc" + "def";

        String s2 = "abc";        // "abc" 在常量池中存在了，s2创建时直接用常量池中"abc"的引用
        char[] ch = new char[]{'a', 'b', 'c'};
        String s1 = new String(ch);     // s1对象并不在常量池中
        s1.intern();                 // s1.intern()；调用之后，会将s1对象的引用放入到常量池中
        System.out.println(s1 == s2);

        final int[] array = {1, 2, 3, 4};
        //array = new int[10];
        array[0] = 88;

        String s = "hello";
        s += " world";
        System.out.println(s);  // 输出：hello world

        long start = System.currentTimeMillis();
        String ss = "";
        for (int i = 0; i < 10000; ++i) {
            ss += i;
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer("");
        for (int i = 0; i < 10000; ++i) {
            sbf.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        StringBuilder sbd = new StringBuilder();
        for (int i = 0; i < 10000; ++i) {
            sbd.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);

    }
}
