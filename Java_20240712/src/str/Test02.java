package str;

public class Test02 {
    public static void main(String[] args) {
        String str = "";
        for (int i = 0; i < 100; i++) {
            str += i;
            //str = str+i;
        }
        System.out.println(str);
        System.out.println("==================");

        String str1 = "abcd";
        for (int i = 0; i < 100; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str1);
            stringBuilder.append(i);
            str1 = stringBuilder.toString();
        }
        System.out.println(str1);
        System.out.println("=============");

        String str2 = "abcd";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        for (int i = 0; i < 100; i++) {
            stringBuilder.append(i);
        }
        System.out.println(stringBuilder);


        String s = "hello";
        //可变的对象
        StringBuilder stringBuilder1 = new StringBuilder(s);
        stringBuilder.append("abcd");
        System.out.println(stringBuilder);

        StringBuffer sb = new StringBuffer();
        sb.append("fdsa");

       /* stringBuilder.reverse();
        System.out.println(stringBuilder);
*/

        /*
        把stringBuilder对象 转变为String对象
        String s = stringBuilder.toString();
        System.out.println(s);
        */
    }
}
