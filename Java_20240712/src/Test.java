public class Test {
    public int firstUniqChar(String s) {
        int[] count = new int[26];
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            count[ch - 'a']++;
        }

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (count[ch - 'a'] == 1) {
                return i;
            }
        }
        return -1;
    }

    //Character.isDigit()
    //Character.isAlphabetic()
    public boolean isEffective(char ch) {
        if (Character.isLetter(ch) || Character.isDigit(ch)) {
            return true;
        }
        return false;
        //return Character.isLetterOrDigit(ch);//是不是数字字符或者字母
    }


    public boolean isPalindrome(String s) {
        s = s.toLowerCase();//全部整体转为小写字符

        int left = 0;
        int right = s.length() - 1;
        while (left < right) {

            while (left < right && !isEffective(s.charAt(left))) {
                left++;
            }
            //left下标一定是 有效的字符
            while (left < right && !isEffective(s.charAt(right))) {
                right--;
            }
            //right下标一定是 有效的字符
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

    public static void main(String[] args) {

    }

    public static void main11(String[] args) {
        String s = "hello";
        //可变的对象
        StringBuilder stringBuilder = new StringBuilder(s);
        stringBuilder.append("abcd");
        System.out.println(stringBuilder);

        StringBuffer sb = new StringBuffer();
        sb.append("fdsa");


       /* stringBuilder.reverse();
        System.out.println(stringBuilder);
*/

        /*
        把stringBuilder对象 转变为String对象
        String s = stringBuilder.toString();
        System.out.println(s);
        */


    }

    public static void main10(String[] args) {
        String str = "abcd";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        for (int i = 0; i < 100; i++) {
            stringBuilder.append(i);
        }
        System.out.println(stringBuilder);
    }

    public static void main9(String[] args) {
        String str = "abcd";
        for (int i = 0; i < 100; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append(i);
            str = stringBuilder.toString();
        }
        System.out.println(str);
    }


    public static void main8(String[] args) {
        String str = "";
        for (int i = 0; i < 100; i++) {
            str += i;
            //str = str+i;
        }
        System.out.println(str);
    }


    public static void main7(String[] args) {
        long start = System.currentTimeMillis();
        String s = "";
        for (int i = 0; i < 10000; ++i) {
            s += i;
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer("");
        for (int i = 0; i < 10000; ++i) {
            sbf.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        StringBuilder sbd = new StringBuilder();
        for (int i = 0; i < 10000; ++i) {
            sbd.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public static void main6(String[] args) {
        String s = "hello";
        s += " world";
        System.out.println(s);  // 输出：hello world
    }


    public static void main5(String[] args) {
        final int[] array = {1, 2, 3, 4};
        //array = new int[10];
        array[0] = 88;


    }

    public static void main4(String[] args) {
        String s2 = "abc";        // "abc" 在常量池中存在了，s2创建时直接用常量池中"abc"的引用
        char[] ch = new char[]{'a', 'b', 'c'};
        String s1 = new String(ch);     // s1对象并不在常量池中
        s1.intern();                 // s1.intern()；调用之后，会将s1对象的引用放入到常量池中
        System.out.println(s1 == s2);
    }


    public static void main3(String[] args) {
        String str = "abc" + "def";
    }

    public static void main2(String[] args) {
        String str1 = "abcd";
        String str2 = "abcd";
        String str3 = new String("abcd");
        String str4 = new String("abcd");

        System.out.println(str1 == str2);

        System.out.println(str3 == str4);

        System.out.println(str1 == str3);

    }

    public static void main1(String[] args) {
        String str = "helloabc";
        System.out.println(str.contains("abc"));
    }
}
