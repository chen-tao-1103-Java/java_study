public class UserNameErrorException extends RuntimeException {
    public UserNameErrorException() {
        super();
    }

    public UserNameErrorException(String message) {
        super(message);
    }
}

