import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            int n = scanner.nextInt();
            System.out.println(n);
        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.out.println("输入的数据不匹配");
        } finally {
            System.out.println("执行了finally部分，一般用来关闭资源！");
        }
    }
}
