public class PassWorldException extends RuntimeException {
    public PassWorldException() {
        super();
    }

    public PassWorldException(String message) {
        super(message);
    }
}
