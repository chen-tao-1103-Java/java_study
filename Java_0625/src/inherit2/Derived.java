package inherit2;

public class Derived extends Base {
    int a;
    int c;

    public void methodA() {
        System.out.println("Derived::methodA()");
    }

    public void methodB() {
        System.out.println("Derived中的methodB()方法");
    }

    public void methodC() {
        methodB();         // 访问子类自己的methodB()
        methodA(10);         // 访问父类继承的methodA()
        // methodD();      // 编译失败，在整个继承体系中没有发现方法methodD()
    }


    public void method() {
        super.a = 10;//此时当 父类和子类 都拥有同名的变量的时候，优先访问子类自己
        b = 20;
        c = 30;
        //d = 100;
        //System.out.println(super.a);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
