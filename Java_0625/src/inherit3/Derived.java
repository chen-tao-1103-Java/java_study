package inherit3;

public class Derived extends Base {

    static {
        System.out.println("子类静态代码块");
    }

    {
        System.out.println("子类代码块");
    }

    public Derived() {
        super();
        System.out.println("子类构造方法");
    }
}
