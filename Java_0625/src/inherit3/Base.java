package inherit3;

public class Base {
    {
        System.out.println("父类代码块");
    }

    public Base() {
        System.out.println("父类构造方法");
    }

    static {
        System.out.println("父类静态代码块");
    }
}


