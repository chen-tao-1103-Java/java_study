package inherit1;

public class Bird extends Animal {

    public Bird(String name, int age) {
        super(name, age);
    }

    public void flying() {
        System.out.println(this.getName() + "在天上飞");
    }
}
