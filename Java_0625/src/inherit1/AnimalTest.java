package inherit1;

public class AnimalTest {
    public static void main(String[] args) {
        Dog dog = new Dog("旺财", 2);
        Cat cat = new Cat("小猫咪", 1);
        Bird bird = new Bird("自在哥", 5);


        System.out.println("姓名：" + dog.getName() + "  " + "年龄：" + dog.getAge());
        System.out.println("姓名：" + cat.getName() + "  " + "年龄：" + cat.getAge());
        System.out.println("姓名：" + bird.getName() + "  " + "年龄：" + bird.getAge());
        System.out.println("========================");


        dog.setAge(3);
        cat.setAge(2);
        bird.setAge(6);
        System.out.println("姓名：" + dog.getName() + "  " + "年龄：" + dog.getAge());
        System.out.println("姓名：" + cat.getName() + "  " + "年龄：" + cat.getAge());
        System.out.println("姓名：" + bird.getName() + "  " + "年龄：" + bird.getAge());
        System.out.println("===============================");

        dog.call();
        cat.catchingMice();
        bird.flying();
    }
}