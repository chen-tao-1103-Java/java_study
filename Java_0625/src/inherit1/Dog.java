package inherit1;

public class Dog extends Animal {

    public Dog(String name, int age) {
        super(name, age);
    }

    public void call() {
        System.out.println(this.getName() + "汪汪叫");
    }
}
