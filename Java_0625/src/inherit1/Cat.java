package inherit1;

public class Cat extends Animal {
    public Cat(String name, int age) {
        super(name, age);
    }

    public void catchingMice() {
        System.out.println(this.getName() + "抓老鼠");
    }
}
