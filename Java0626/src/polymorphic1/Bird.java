package polymorphic1;

public class Bird extends Animal {

    public Bird(String name, int age) {
        super(name, age);
    }

    public void flying() {
        System.out.println("小鸟正在飞");
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "吃鸟粮");
    }

    @Override
    public void walk() {
        System.out.println(this.getName() + "散步");
    }
}
