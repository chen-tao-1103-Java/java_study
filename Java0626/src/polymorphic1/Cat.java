package polymorphic1;

public class Cat extends Animal {

    public Cat(String name, int age) {
        super(name, age);
    }

    public void catchingMice() {
        System.out.println("猫抓老鼠");
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "吃猫粮");
    }

    @Override
    public void walk() {
        System.out.println(this.getName() + "散步");
    }
}
