package polymorphic1;

public class Dog extends Animal {
    public Dog(String name, int age) {
        super(name, age);
    }

    public void call() {
        System.out.println("小狗汪汪叫");
    }

    @Override
    public void eat() {
        System.out.println(this.getName() + "吃狗粮");
    }

    @Override
    public void walk() {
        System.out.println(this.getName() + "散步");
    }
}
