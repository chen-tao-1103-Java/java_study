package polymorphic1;

public class AnimalTest {

    public static void eat(Animal animal) {
        animal.eat();
    }

    public static void walk(Animal animal) {
        animal.walk();
    }

    public static void main(String[] args) {
//        Animal animal1 = new Dog("旺财", 10);
//        Animal animal2 = new Cat("小猫咪", 12);
//        Animal animal3 = new Bird("自由哥", 2);
//        animal1.eat();
//        animal2.eat();
//        animal3.eat();
//        System.out.println("======================");

        eat(new Dog("旺财", 10));
        eat(new Cat("小猫咪", 12));
        eat(new Bird("自由哥", 2));
        System.out.println("====================");


//        animal1.walk();
//        animal2.walk();
//        animal3.walk();
        walk(new Bird("旺财", 10));
        walk(new Cat("小猫咪", 12));
        walk(new Bird("自由哥", 2));
        System.out.println("======================");

        Animal[] animals = {
                new Bird("小鸟", 2),
                new Cat("米奇", 5),
                new Dog("发财", 3)
        };

        for (Animal animal : animals) {
            eat(animal);
        }
        System.out.println("=====================");

        for (Animal animal : animals) {
            walk(animal);
        }
    }
}
