package polymorphic2;

public class ShapeTest {
    public static void drawShape(Shape shape) {
        shape.draw();
    }

    public static void main(String[] args) {
        Shape[] shapes = {
                new Cycle(),
                new Rect(),
                new Flower(),
                new Triangle()
        };

        for (Shape shape : shapes) {
            shape.draw();
        }
    }
}
