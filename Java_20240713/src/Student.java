public class Student implements Comparable<Student> {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        if (this.age - o.age > 0) {
            return 1;
        } else if (this.age - o.age < 0) {
            return -1;
        } else {
            return 0;
        }
        //return 0;
    }
}
