import java.util.Arrays;

public class Test01 {

    public static void bubbleSort(Comparable[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    Comparable tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("bit", 10);
        students[1] = new Student("hello", 40);
        students[2] = new Student("gbc", 5);

        bubbleSort(students);

        System.out.println(Arrays.toString(students));

    }

    public static void main6(String[] args) {
        Student student1 = new Student("bit", 10);
        Student student2 = new Student("hello", 40);

        AgeComparator ageComparator = new AgeComparator();

        if (ageComparator.compare(student1, student2) > 0) {
            System.out.println("student1 > student2");
        } else {
            System.out.println("student1 < student2");
        }
    }


    public static void main5(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("bit", 10);
        students[1] = new Student("hello", 40);
        students[2] = new Student("gbc", 5);

        //AgeComparator ageComparator = new AgeComparator();
        NameComparator nameComparator = new NameComparator();
        Arrays.sort(students, nameComparator);

        System.out.println(Arrays.toString(students));
    }

    public static void main4(String[] args) {
        Student student1 = new Student("bit", 10);
        Student student2 = new Student("hello", 40);

        if (student1.compareTo(student2) > 0) {
            System.out.println("student1 > student2");
        } else {
            System.out.println("student1 < student2");
        }
    }

    public static void main2(String[] args) {
        String[] strings = {"abc", "hello", "bcd"};
        Arrays.sort(strings);
        System.out.println(Arrays.toString(strings));
    }

    public static void main3(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("bit", 10);
        students[1] = new Student("hello", 40);
        students[2] = new Student("gbc", 5);

        Arrays.sort(students);

        System.out.println(Arrays.toString(students));
    }

    public static void main1(String[] args) {
        int[] array = {1, 4, 2, 7, 3, 8, 5};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }
}
