public class Test02 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person();
        Person person2 = (Person) person.clone();
        person2.money.m = 1999;

        System.out.println("person：" + person.money.m);
        System.out.println("person2：" + person2.money.m);
    }
}
